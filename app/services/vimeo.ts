import {IVideo} from './videoInterface';
import {IVideoItem} from './videoItemInterface';
import {VimeoValidator, IVideoValidator} from '../validators/validators';

export default class Vimeo implements IVideo<any>
{

  private static URL_VIDEO: string = 'https://api.vimeo.com/videos/$VID$';
  private static API_KEY: string = 'eb32577ececac419eeecf9e998b3890b';
  private static AUTH_TYPE: string = 'bearer';
  
  private validator: IVideoValidator;

  static $inject = ['$http'];

  constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    this.validator = new VimeoValidator();
  }

  /**
   * @returns {string}
   */
  private static getAuthHeader() {
    return Vimeo.AUTH_TYPE + " " + Vimeo.API_KEY;
  }

  /**
   * @param ID
   * @returns {ng.IPromise}
   */
  public fetchVideo(ID: string): ng.IPromise<IVideoItem> {
    return this.$http
      .get(Vimeo.createVideoUrl(this.validator.getId(ID)), {
        headers: {
          Authorization: Vimeo.getAuthHeader()
        }
      })
      .then((response) :IVideoItem =>{
        var videoData = response.data;
        return <IVideoItem>{
          videoId: this.validator.getId(ID),
          type: 2,
          thumbnail: videoData['pictures']['sizes'][1]['link'],
          likes: videoData['metadata']['connections']['likes']['total'],
          plays: videoData['stats']['plays'],
          title: videoData['name'],
          duration: videoData['duration'],
          creationDate: videoData['created_time'],
          starred: false,
          embed: videoData['embed']['html']
        };
      }, (error): Error=>{
        return Error('Service problem');
      });
  }

  /**
   * @param ID
   * @returns {string}
   */
  private static createVideoUrl(ID: string): string {
    return Vimeo.URL_VIDEO.replace('$VID$', ID);
  }
  
  /**
   * @param URL
   * @returns {boolean}
   */
  validateUrl(url: string): boolean {
    return this.validator.validate(url);
  }
}