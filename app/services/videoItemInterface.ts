export interface IVideoItem {
  videoId: string,
  type: number,
  thumbnail: string,
  duration: string,
  likes: number,
  plays: number,
  title: string,
  creationDate: number,
  starred: boolean,
  embed: string
}