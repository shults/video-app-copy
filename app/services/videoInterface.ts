export interface IVideo<T> {
  validateUrl(videoURL: string): boolean;
  fetchVideo(ID: string): ng.IPromise<T>;
}