import TemplateHelper from '../helpers/template';
import {IVideoItem} from '../services/videoItemInterface';
import ModalController from './modal';
import DbService from '../services/db';

enum ViewType {List, Tiles}

export default class DefaultController {

  static $inject = ['$scope', '$mdDialog', 'DB', 'video', '$sce'];

  constructor($scope, $mdDialog, DB: DbService, video) {
    $scope.view = <ViewType> ViewType.List;
    
    $scope.showAlert = function() {
      $mdDialog.show({
        controller: ModalController,
        template: TemplateHelper.load('add_video'),
        clickOutsideToClose: false,
      });
    };

    $scope.videos = function videos() {
      return DB.findAll();
    };
    
    $scope.toggleButtonLabel = function toggleButtonLabel() {
      return 'Show ' + ($scope.view === ViewType.List ? 'tiles' : 'list');
    };

    $scope.toggleView = function toggleView() {
      $scope.view = ($scope.view === ViewType.List) ? ViewType.Tiles : ViewType.List;
    };

    $scope.showList = function() {
      return $scope.view === ViewType.List;
    };

    $scope.showTiles = function() {
      return $scope.view === ViewType.Tiles;
    };
    
    $scope.flush = function() {
      DB.flush();
    }
     
    $scope.play = function(item) {
      $mdDialog.show({
        controller: VideoPlayerController,        
        template: TemplateHelper.load('play_video'),
        clickOutsideToClose: true,
        locals: {
          item: item
        }
      });     
    }

    //Do wydzielenia na zewnatrz w poniedzialek jako VideoPlayerController i jako filter Trusted
    function VideoPlayerController($scope, $mdDialog, item, $sce) {
        $scope.item = item;
        $scope.getIframeUrl = function() {
          var item = $scope.item;
          var url = (item.type === 1) ? 'https://www.youtube.com/embed/'+item.videoId : 'https://player.vimeo.com/video/'+item.videoId+'?badge=0&autopause=0&player_id=0';
          return $sce.trustAsResourceUrl(url);
        }
        
        $scope.closeDialog = function() {
          $mdDialog.hide();
        }
      }
  }     
}       
          
          
        