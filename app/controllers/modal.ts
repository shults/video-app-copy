import {IVideoItem} from '../services/videoItemInterface';

export default class ModalController {
  
  static $inject = ['$scope', '$mdDialog', 'DB', 'video'];
  
  constructor($scope, $mdDialog, DB, video) {
    
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    
    $scope.add = function() {
      video
        .fetchVideo($scope.video.url)
        .then((data: IVideoItem) => {
          DB.addItem(data);
        });
  
      $scope.cancel();
    };
  }
  
}