import {IVideo} from '../services/videoInterface';

function validateVideoUrl(Video: IVideo<any>) : Function {
  return (url: string) : boolean => {
    return Video.validateUrl(url);
  };
};

validateVideoUrl.$inject = ['video'];

export default validateVideoUrl;