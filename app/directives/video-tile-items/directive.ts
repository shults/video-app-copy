declare function require(path: string): string;

export default function appVideoTileItems() {
  return {
    restrict: 'E',
    template: require('./template.html')
  }
}